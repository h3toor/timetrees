﻿using System;
using System.IO;
using Newtonsoft.Json;
using System.Globalization;
using System.Collections.Generic;


namespace TimeTrees
{
    class Program
    {
        const int PeopleIdIndex = 0;
        const int PeopleNameIndex = 1;
        const int BirthDateIndex = 2;
        const int DeathDateIndex = 3;
        const int DateOfEventIndex = 0;
        const int NameOfEventIndex = 1;

        struct Person
        {
            public int Id;
            public string Name;
            public DateTime BirthDate;
            public DateTime DeathDate;
        }
        struct TimelineEvent
        {
            public DateTime Date;
            public string someEvent;            
        }
        static void Main(string[] args)
        {
            string peopleJsonFile = "..\\..\\..\\..\\people.json";
            string peopleCsvFile = "..\\..\\..\\..\\people.csv";
            string timelineJsonFile = "..\\..\\..\\..\\timeline.json";
            string timelineCsvFile = "..\\..\\..\\..\\timeline.csv";
            WritePeopleJson(peopleJsonFile);
            WritePeopleCsv(peopleCsvFile);
            WriteTimelineJson(timelineJsonFile);
            WriteTimelineCsv(timelineCsvFile);
            (Person[] peopleJson, TimelineEvent[] timeTreesJson) = FromFileToArray(peopleJsonFile, timelineJsonFile);
            TimelineEvent[] timeTreesCsv = ReadEventData(timelineCsvFile);
            Person[] peopleCsv = ReadPeopleData(peopleCsvFile);
            (int years, int months, int days) = DeltaMinAndMaxDate(timeTreesCsv);

            Console.WriteLine($"Между максимальными и минимальными датами прошло  :{years} лет,{months} месяцев и {days} дней");
            CheckAge(peopleCsv);
        }
        static TimelineEvent[] ReadEventData(string text)
        {
            string[] textToLines = File.ReadAllLines(text);
            TimelineEvent[] timelineEvent = new TimelineEvent[textToLines.Length];
            for (int i = 0; i < textToLines.Length; i++)
            {
                string line = textToLines[i];
                string[] splitEvent = line.Split(";");
                string dateOfEvent = splitEvent[DateOfEventIndex];
                timelineEvent[i].Date = CorrectDate(dateOfEvent);
                timelineEvent[i].someEvent = splitEvent[NameOfEventIndex];
            }
            return timelineEvent;
        }
        static DateTime CorrectDate(string someDate)
        {
            if (someDate.Length == 4)
                someDate += "-01";
            return DateTime.Parse(someDate);
        }
        static Person[] ReadPeopleData(string text)
        {
            string[] textToLines = File.ReadAllLines(text);
            Person[] splitPeople = new Person[text.Length];
            for (int d = 0; d < textToLines.Length; d++)
            {
                string line = textToLines[d];
                string[] parts = line.Split(";");
                splitPeople[d].Id = Int32.Parse(parts[PeopleIdIndex]);
                splitPeople[d].Name = parts[PeopleNameIndex];
                splitPeople[d].BirthDate = DateTime.Parse(parts[BirthDateIndex]);
                if (splitPeople.Length == 4)
                {
                    splitPeople[d].DeathDate = DateTime.Parse(parts[DeathDateIndex]);
                }
            }
            return splitPeople;
        }
        static (DateTime, DateTime) FindMinAndMaxDate(TimelineEvent[] timeline)
        {
            DateTime minDate = DateTime.MaxValue, maxDate = DateTime.MinValue;
            for(int t = 0; t < timeline.Length; t++)
            {
                if (timeline[t].Date.CompareTo(maxDate) > 0)
                    maxDate = timeline[t].Date;
                if (timeline[t].Date.CompareTo(minDate) < 0)
                    minDate = timeline[t].Date;
            }
            return (maxDate, minDate);

        }
        static (int, int, int) DeltaMinAndMaxDate(TimelineEvent[] timeline)
        {
            (DateTime maxDate, DateTime minDate) = FindMinAndMaxDate(timeline);
            return (maxDate.Year - minDate.Year, maxDate.Month - minDate.Month, maxDate.Day - minDate.Day);
        }
        static void CheckAge(Person[] people)
        {
            Console.WriteLine("Имена людей, которые родились в високосный год и их возраст меньше 20 лет");
            for (int i = 0; i < people.Length; i++)
            {
                DateTime date = people[i].BirthDate;
                var dateToday = DateTime.Now;
                if (DateTime.IsLeapYear(date.Year) && (dateToday.Year - date.Year <= 20)) 
                {
                    Console.WriteLine($"{people[i].Name}");
                }
            }
        }
        static (Person[], TimelineEvent[]) FromFileToArray(string peopleFile, string timelineFile)
        {
            List<Person> textPeopleJson = ReadPeopleJson(peopleFile);
            Person[] people = new Person[textPeopleJson.Count];
            textPeopleJson.CopyTo(people);
            List<TimelineEvent> timelineEvent = ReadEventJson(timelineFile);
            TimelineEvent[] timeTrees = new TimelineEvent[timelineEvent.Count];
            timelineEvent.CopyTo(timeTrees);
            return (people, timeTrees);
        }
        static List<TimelineEvent> ReadEventJson(string file)
        {
            string[] text1 = File.ReadAllLines(file);
            for (int i = 2; (i < text1.Length - 2); i =+ 4)
            {
                if (text1[i] == null);
                {
                    string line = text1[i];
                    string[] date = line.Split(":");
                    if (date[1].Length == 8)
                    {
                        date[1] = date[1].Insert(6, "-01");
                    }

                    date[0] += ":";
                    text1[i] = date[0] + date[1];
                }
            }
            string text = String.Join(null, text1);
            return JsonConvert.DeserializeObject<List<TimelineEvent>>(text);
        }

        static List<Person> ReadPeopleJson(string file)
        {
            string text = File.ReadAllText(file);
            string nullDate = "\"0001-01-01\" ";
            text = text.Replace("null", nullDate);
            return JsonConvert.DeserializeObject<List<Person>>(text);
        }
        static void WriteTimelineCsv(string file)
        {
            string[] text = new string[]
            {
                "1939;World War Two",
                "1991-12-19;Распад Союза",
                "2020 -03-01;Covid s**t"
            };
            File.WriteAllLines(file, text);
        }
        static void WritePeopleCsv(string file)
        {
            string[] text = new string[]
            {
                "1;НеЯрослав;2004 - 02 - 20",
                "2;Имя 2;1937 - 10 - 01; 2001 - 07 - 22",
                "3;Имя 4;2015 - 12 - 22",
                "4;Имя 27;2021 - 07 - 01" 
            };
            File.WriteAllLines(file, text);
        }    
        static void WriteTimelineJson(string file)
        {
            TimelineEvent[] timeline = new TimelineEvent[2];
            timeline[0].Date = DateTime.Parse("2000 - 06 - 05");
            timeline[0].someEvent = "какое-то событие 0";
            timeline[1].Date = DateTime.Parse("2001 - 11 - 11");
            timeline[1].someEvent = "какое-то событие 2";
            List<TimelineEvent> list = new List<TimelineEvent>(2) { timeline[0], timeline[1] };
            string text = JsonConvert.SerializeObject(list);
            File.WriteAllText(file, text);
        }
        static void WritePeopleJson(string file)
        {
            Person[] people = new Person[3];
            people[0].Id = 1;
            people[0].Name = "stariy kommunist";
            people[0].BirthDate = DateTime.Parse("1922-11-07");
            people[0].DeathDate = DateTime.Parse("1991-12-29");
            people[1].Id = 2;
            people[1].Name = "Chelgu";
            people[1].BirthDate = DateTime.Parse("1976-10-04");
            people[1].DeathDate = DateTime.MinValue;
            people[2].Id = 3;
            people[2].Name = "Ярослав";
            people[2].BirthDate = DateTime.Parse("2003-02-20");
            people[2].DeathDate = DateTime.MinValue;
            List<Person> list = new List<Person>(2) {people[0], people[1]};
            string text = JsonConvert.SerializeObject(list);
            File.WriteAllText(file, text);
        }

    }
}

